require_relative 'lib/linter/version'

Gem::Specification.new do |spec|
  spec.name          = 'linter'
  spec.version       = Linter::VERSION
  spec.authors       = ['lien van den steen']
  spec.email         = ['lienvandensteen@gmail.com']

  spec.summary       = 'Library to check a text for gender coded language'
  # spec.description   = %q{TODO: Write a longer description or delete this line.}
  spec.homepage      = 'https://gitlab.com/lienvdsteen/linter'
  spec.license       = 'MIT'

  if spec.respond_to?(:metadata)
    spec.metadata['homepage_uri'] = 'https://gitlab.com/lienvdsteen/linter'
    spec.metadata['source_code_uri'] = 'https://gitlab.com/lienvdsteen/linter'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files         = Dir['{data,lib}/**/*', 'LICENSE.txt', 'README.md']
  spec.bindir        = 'bin'
  spec.executables   = ['linter']
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.1'
  spec.add_development_dependency 'pry', '~> 0.13'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.85'
  spec.add_development_dependency 'rubocop-packaging', '~> 0.1'
  spec.add_development_dependency 'rubocop-rspec', '~> 1.39'

  spec.add_dependency('colorize', '~> 0.8')
end
