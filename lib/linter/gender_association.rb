# frozen_string_literal: true

module Linter
  class GenderAssociation < BaseAssociation
    FULL_WORD = false

    private

    def self.wordlists
      file_path = File.join(__dir__,'../../data/gender_association_wordlist.yml')
      @wordlists ||= YAML.load_file(file_path)
    end

    def self.calculate_trend(result)
      case result.feminine_coded_word_counts.values.sum - result.masculine_coded_word_counts.values.sum
      when 0
        'neutral'
      when 1..3
        'feminine-coded'
      when 3..Float::INFINITY
        'strongly feminine-coded'
      when -Float::INFINITY..-3
        'strongly masculine-coded'
      else
        'masculine-coded'
      end
    end

    def self.add_recommendation(result)
      recommendation_file.find { |rec| rec['type'] == 'gender' }&.dig('trends')&.find { |trend| trend['result'] == result.trend }&.dig('recommendation')
    end
  end
end
