# frozen_string_literal: true

require_relative 'linter/version'
require_relative 'linter/base_association'
require_relative 'linter/gender_association'
require_relative 'linter/pronoun_association'
require_relative 'linter/cli'
require_relative 'linter/misused_words'
require_relative 'linter/mindset_association'

require 'yaml'
require 'colorize'
require 'ostruct'

module Linter
  class Error < StandardError; end

  class << self
    def analyze(text, job_ad: false)
      response = {
        gender_association_analysis: Linter::GenderAssociation.analyze(text),
        pronoun_analysis: Linter::PronounAssociation.analyze(text),
        misused_words_analysis: Linter::MisusedWords.analyze(text)
      }
      response[:mindset_association_analysis] = Linter::MindsetAssociation.analyze(text) if job_ad
      response
    end

    def sources
      file_path = File.join(__dir__, '../data/sources.yml')
      YAML.load_file(file_path)
    end
  end
end
