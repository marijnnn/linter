# Linter

Inspired by [Gender Decoder](http://gender-decoder.katmatfield.com/) this Gem analyzed a given text for gender associated language.

Currently this Gem only works for English text but open to contributions for other languages.


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'linter'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install linter

## Usage

If you want to perform all of the inclusiveness checks we currently offer, you can run:

```ruby
text = 'your text here'
response = Linter.analyze(text)
```

If you want to include the checks specifically for job ads as well, run it like this:

```ruby
text = 'your text here'
response = Linter.analyze(text, job_ad: true)
```

You can also use the checks individually:

```ruby
text = 'Collaborate closely with the manager. Analytics all the way.'
Linter::GenderAssociation.analyze(text)
# #<OpenStruct feminine_coded_word_counts={"collaborate" => 1}, masculine_coded_word_counts={"analytics" => 1}, trend="neutral">

text = 'He was working at the bar.'
Linter::PronounAssociation.analyze(text)
#<OpenStruct trend="masculine-coded", feminine_coded_word_counts={}, masculine_coded_word_counts={"he"=>1}>

text = 'You are my spirit animal'
Linter::MisusedWords.analyze(text)
#<OpenStruct misused_words=[{"word"=>"spirit animal", "reason"=>"The problem is that spirit animals are an important part of the belief\nsystem of some cultures and refer to a spirit that “helps guide or protect\na person on a journey and whose characteristics that person shares or\nembodies.” Referring to something as your spirit animal is cultural\nappropriation. Avoid using it.\n", "replace_with"=>["kindred spirit", "raison d'etre"]}], trend="">
```

You'll notice that the `association` checks do a comparison of the whole text and will determine a _trend_ for your text. When your text uses a lot of
masculine-coded language, the text will be marked as such. The regular checks (like `MisusedWords`) will return all the misused words, a reason why and if possible, we provide some suggestions to replace the word.

We've also included a `recommendation` where possible. This is to help guide people what to do with the results from the
trend.

## CLI Usage

```console
linter path/to_file/example.md
#<OpenStruct feminine_coded_word_counts={}, masculine_coded_word_counts={"analytical"=>1}, trend="strongly masculine-coded">
#<OpenStruct feminine_coded_word_counts={}, masculine_coded_word_counts={"he"=>1}, trend="masculine-coded">
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/lienvdsteen/linter. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

We are looking for different types of contributions:
- additional checks
- adding/editing words to the existing wordlists
- support for other languages besides English
- ...

If you add any checks or words, please also add the sources used to the `sources.yml` file.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Linter project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/lienvdsteen/linter/blob/master/CODE_OF_CONDUCT.md).
