# frozen_string_literal: true

RSpec.describe Linter do
  it 'has a version number' do
    expect(Linter::VERSION).not_to be nil
  end

  describe '.analyze' do
    let(:text) { 'Collaborate closely Collaborate collaboratively with the affectionate . Analytics all the way.'}

    it 'performs an analysis on all the checks' do
      result = described_class.analyze(text)
      expect(result.keys).to eq(%i[gender_association_analysis pronoun_analysis misused_words_analysis])
    end

    it 'performs the job ad check as well when the flag is enabled' do
      result = described_class.analyze(text, job_ad: true)
      expect(result.keys).to eq(%i[gender_association_analysis pronoun_analysis misused_words_analysis mindset_association_analysis])
    end
  end

  describe '.sources' do
    it 'returns the sources' do
      result = described_class.sources
      expect(result.first.keys).to eq(%w[title url])
    end
  end
end
