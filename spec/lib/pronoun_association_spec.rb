# frozen_string_literal: true

require 'spec_helper'

describe Linter::PronounAssociation do
  let(:text) { 'He works closely with the heroic manager.'}

  describe '.analyze' do
    it 'returns the feminine associated pronouns' do
      result = described_class.analyze(text)
      expect(result.feminine_coded_word_counts).to eq({})
    end

    it 'returns the masculine associated words' do
      result = described_class.analyze(text)
      expect(result.masculine_coded_word_counts).to eq({'he' => 1})
    end

    it 'calculates a trend' do
      result = described_class.analyze(text)
      expect(result.trend).to eq('masculine-coded')
    end
  end
end
