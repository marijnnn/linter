# frozen_string_literal: true

require 'spec_helper'

describe Linter::MindsetAssociation do
  let(:text) { 'We are striving to a driven culture. The brightest, smartest performers will win.'}

  describe '.analyze' do
    it 'returns the growth coded words' do
      result = described_class.analyze(text)
      expect(result.growth_coded_word_counts).to eq({'driven' => 1, 'striving' => 1})
    end

    it 'returns the fixed coded words' do
      result = described_class.analyze(text)
      expect(result.fixed_coded_word_counts).to eq({'brightest' => 1, 'performers' => 1, 'smartest' => 1})
    end

    it 'calculates a trend' do
      result = described_class.analyze(text)
      expect(result.trend).to eq('fixed-coded')
    end

    it 'includes a recommendation' do
      result = described_class.analyze(text)
      expect(result.recommendation).not_to be_nil
    end
  end
end
